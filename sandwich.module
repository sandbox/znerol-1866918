<?php
/**
 * @file
 * Provide API for postprocessing formatted fields.
 */

define('SANDWICH_SLICER_PREG_PATTERN_DEFAULT', '/^(?=<p>)/m');

/**
 * Returns HTML for a sandwichable field.
 *
 * This is the default theme implementation to render sliced up field contents.
 * Theme developers who are comfortable with overriding theme functions may do
 * so in order to customize this markup. This function can be overridden with
 * varying levels of specificity. For example, for a field named 'body'
 * displayed on the 'article' content type, any of the following functions will
 * override this default implementation. The first of these functions that
 * exists is used:
 * - THEMENAME_sandwich__body__article()
 * - THEMENAME_sandwich__body()
 * - THEMENAME_sandwich()
 *
 * @param $variables
 *   An associative array containing:
 *   - slices: An array of strings derived from slicing up the markup of a
 *     field.
 *   - slices_field_name: A string containing the name of the field from which
 *     the sliced markup was derived from.
 *   - slices_field_delta: A number indicating the delta of the field value.
 *   - slice_prefix: An array of render arrays indexed by slice number of
 *     content which will be prepended to the specified slices. See detalied
 *     description below.
 *   - slice_suffix: An array of render arrays indexed by slice number of
 *     content which will be appended to the specified slices. See detailed
 *     description below.
 *   - slice_prefix_fields: An array of arrays containing field names indexed
 *     by slice number of fields which will be prepended to the specified
 *     slices.
 *   - slice_suffix_fields: An array of arrays containing field names indexed
 *     by slice number of fields which will be appended to the specified slice.
 *   - slicer: The function used to slice up the content as a string.
 *   - entity: The entity object this field belongs to.
 *   - entity_output: The structured content array tree for all of the entity's
 *     fields.
 *   - entity_type: The type of entity; for example, 'node' or 'user'.
 *   - bundle: The bundle of the entity; for example, 'article'. For entities
 *     without bundle this defaults to the entity type.
 *   - view_mode: View mode; for example, 'full' or 'teaser'.
 *   - display: An array of display settings, as found in the 'display' entry
 *     of instance definitions. See field_view_field for detailed information.
 *
 * In order to inject arbritrary markup into sliced field content, implement a
 * preprocess function following this pattern belowe. Note that if you implement
 * the function like this, the markup will be injected in every sliced field on
 * every entity type before the third slice. Note also that if there are less
 * than three slices, the markup will not be inserted.
 *
 *     function MYTHEME_preprocess_sandwich(&$variables) {
 *       // Insert banner tag before third slice.
 *       $variables['slice_prefix'][2]['mybanner'] = array(
 *         '#markup' => '<div>Put your banner tag here</div>',
 *       );
 *     }
 *
 * In the following example, the image field is always placed before the second
 * last slice:
 *
 *     function MYTHEME_preprocess_sandwich(&$variables) {
 *       // Insert field_image after the second last slice.
 *       $slice = max(0, count($variables['slices']) - 3);
 *       $variables['slice_suffix_fields'][$slice][] = 'field_image';
 *     }
 *
 * Tip: Examine slices_field_name, entity_type and bundle in order to restrict where
 * your custom code gets applied.
 *
 * @see template_preprocess_sandwich()
 * @see template_process_sandwich()
 *
 * @ingroup themeable
 */
function theme_sandwich($variables) {
  $result = '';

  // Iterate over slices
  foreach ($variables['slices'] as $i => $markup) {
    // Render injected fields and markup before the slice
    $result .= render(sandwich_build_injects($variables['slice_prefix'], $i));

    // Add the markup from the slide.
    $result .= $markup;

    // Render injected fields and markup after the slice
    $result .= render(sandwich_build_injects($variables['slice_suffix'], $i));
  }

  return $result;
}

/**
 * Render injected fields and markup at the given offset.
 */
function sandwich_build_injects($injects, $i) {
  $build = array();

  if (!empty($injects[$i])) {
    $build += $injects[$i];
  }
  if (!empty($injects['each'])) {
    $build += $injects['each'];
  }

  return $build;
}

/**
 * Theme preprocess function for theme_sandwich().
 *
 * @see theme_sandwich()
 */
function template_preprocess_sandwich(&$variables) {
  // Add specific suggestions that can override the default implementation.
  $variables['theme_hook_suggestions'] = array(
    'sandwich__' . $variables['slices_field_name'],
    'sandwich__' . $variables['slices_field_name'] . '__' . $variables['bundle'],
  );
}

/**
 * Process variables before a sandwiched field is rendered.
 *
 * Move render-arrays of fields named via slice_prefix_fields and slice_suffix_fields to slice_prefix
 * and slice_suffix respectively.
 */
function template_process_sandwich(&$variables) {
  foreach (array('slice_prefix', 'slice_suffix') as $xfix) {
    ksort($variables[$xfix . '_fields']);
    foreach ($variables[$xfix . '_fields'] as $pos => $field_names) {
      foreach ($field_names as $field_name) {
        if ($field_name != $variables['slices_field_name'] && !empty($variables['entity_output'][$field_name])) {
          $variables[$xfix][$pos][$field_name] = $variables['entity_output'][$field_name];
          unset($variables['entity_output'][$field_name]);
        }
      }
    }
  }
}

/**
 * Implement hook_theme().
 */
function sandwich_theme() {
  return array(
    'sandwich' => array(
      'variables' => array(
        'slices' => array(),
        'slices_field_name' => NULL,
        'slices_field_delta' => 0,
        'slice_prefix' => array(),
        'slice_suffix' => array(),
        'slice_prefix_fields' => array(),
        'slice_suffix_fields' => array(),
        'slicer' => NULL,
        'entity' => NULL,
        'entity_output' => array(),
        'entity_type' => NULL,
        'bundle' => NULL,
        'view_mode' => NULL,
        'display' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function sandwich_field_formatter_info_alter(&$info) {
  foreach ($info as $formatter_key => &$formatter) {
    // Fix formatters that have an invalid definition by not defining the
    // settings property or the settings property is not set to an array.
    if (!isset($formatter['settings']) || !is_array($formatter['settings'])) {
      $formatter['settings'] = array();
    }
    $formatter['settings'] += array(
      'sandwich_slicer' => '',
      'sandwich_slicer_preg_pattern' => SANDWICH_SLICER_PREG_PATTERN_DEFAULT,
    );
  }
}

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function sandwich_field_formatter_settings_summary_alter(&$summary, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  // Only set summary for supported fields
  if (!sandwich_slicer_options($display['type'])) {
    return;
  }

  if (!empty($summary)) {
    $summary .= '<br />';
  }

  if (empty($settings['sandwich_slicer'])) {
    $summary .= t('Do not slice field content');
    return;
  }

  $slicer = $settings['sandwich_slicer'];
  $info = sandwich_slicer_info();

  if (empty($info[$slicer])) {
    $summary .= t('Cannot slice field content: Slicer @slicer missing', array('@slicer' => $slicer));
    return;
  }

  if(empty($info[$slicer]['summary'])) {
    $summary .= t('Slice up field content using slicer @slicer',
      array('@slicer' => $info[$slicer]['label']));
  }
  else {
    $placeholders = array();
    foreach ($settings as $key => $value) {
      if (is_string($value)) {
        $placeholders['%' . $key] = $value;
      }
    }
    $summary .= format_string($info[$slicer]['summary'], $placeholders);
  }
}

/**
 * Implements hook_field_formatter_settings_form_alter().
 */
function sandwich_field_formatter_settings_form_alter(&$settings_form, $context) {
  $display = $context['instance']['display'][$context['view_mode']];
  $settings = $display['settings'];

  $options = sandwich_slicer_options($display['type']);
  if (!empty($options)) {
    $settings_form['sandwich_slicer'] = array(
      '#type' => 'select',
      '#title' => t('Slice up field content'),
      '#options' => $options,
      '#default_value' => $settings['sandwich_slicer'],
      '#empty_value' => '',
    );

    $this_field_name = $context['field']['field_name'];
    $settings_form['sandwich_slicer_preg_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Pattern'),
      '#default_value' => $settings['sandwich_slicer_preg_pattern'] ?: SANDWICH_SLICER_PREG_PATTERN_DEFAULT,
      '#states' => array(
        'visible' => array(
          'select[name="fields[' . $this_field_name . '][settings_edit_form][settings][sandwich_slicer]"]' => array('value' => 'sandwich_slicer_preg'),
        ),
      ),
      '#description' => t('Specify a <a href="!url">PCRE compatible pattern</a> which will be used to slice up the field content. The default is %pattern', array('!url' => 'http://php.net/manual/en/reference.pcre.pattern.syntax.php', '%pattern' => SANDWICH_SLICER_PREG_PATTERN_DEFAULT)),
    );
  }
}

/**
 * Implements hook_field_attach_view_alter().
 *
 * Perform the injection callback if requested and hide the current field.
 */
function sandwich_field_attach_view_alter(&$output, $context) {
  // Extract the bundle name
  $entity_type = $context['entity_type'];
  list(, , $bundle_name) = entity_extract_ids($entity_type, $context['entity']);
  if (empty($bundle_name)) {
    // entity_extract_ids returns NULL when entity does not have any bundles.
    // In this case we need to set the entity_type as the bundle name.
    $bundle_name = $entity_type;
  }

  // Loop though each of the fields and check if field content should be
  // sliced up.
  foreach (element_children($output) as $field_name) {
    if (empty($output[$field_name]['#field_type'])) {
      continue;
    }

    // Extract display settings
    $view_mode = $context['view_mode'];
    if ($view_mode == '_custom') {
      $display = $context['display'];
    }
    else {
      $instance = field_info_instance($entity_type, $field_name, $bundle_name);
      $display = empty($instance['display'][$view_mode]) ? $instance['display']['default'] : $instance['display'][$view_mode];
    }
    $settings = $display['settings'];

    if (empty($settings['sandwich_slicer'])) {
      continue;
    }

    // Extract slicer from field settings
    $slicer = $settings['sandwich_slicer'];
    $slicers = sandwich_slicer_info();
    if (empty($slicers[$slicer]) || !function_exists($slicer)) {
      continue;
    }

    // Extract path to the markup inside the field render-array
    $formatter_type = $display['type'];
    if (empty($slicers[$slicer]['field_formatters'][$formatter_type])) {
      continue;
    }
    $keypath = $slicers[$slicer]['field_formatters'][$formatter_type];

    // Loop through field deltas
    foreach (element_children($output[$field_name]) as $delta) {
      $text = drupal_array_get_nested_value($output[$field_name][$delta],
        $keypath, $key_exists);

      if (!$key_exists) {
        continue;
      }

      // Slice up content
      $slices = $slicer($text, $display);

      // Let the themers have a stab on the snack
      $variables = array(
        'slices' => $slices,
        'slices_field_name' => $field_name,
        'slices_field_delta' => $delta,
        'slicer' => $slicer,
        'entity' => $context['entity'],
        'entity_output' => &$output,
        'entity_type' => $entity_type,
        'bundle' => $bundle_name,
        'view_mode' => $view_mode,
        'display' => $display,
      );

      drupal_array_set_nested_value($output[$field_name][$delta],
        $keypath, theme('sandwich', $variables));
    }
  }
}

/**
 * Return a list of slicer options for the given instance.
 */
function sandwich_slicer_options($formatter_type) {
  $info = sandwich_slicer_info();
  $options = array();
  foreach ($info as $callback => $slicer) {
    if (isset($slicer['field_formatters'][$formatter_type])) {
      $options[$callback] = $slicer['label'];
    }
  }

  return $options;
}

/**
 * return a list of slicers built up by invoking hook_slicer_info().
 */
function sandwich_slicer_info() {
  $info = &drupal_static(__FUNCTION__);

  if (!isset($info)) {
    $info = module_invoke_all('sandwich_slicer_info');
    drupal_alter('sandwich_slicer_info', $info);
  }

  return $info;
}

// ----------- Default slicer implementations ----------

/**
 * Implementation of hook_sandwich_slicer_info().
 */
function sandwich_sandwich_slicer_info() {
  return array(
    'sandwich_slicer_preg' => array(
      'label' => t('Using regular expression'),
      'summary' => t('Slice up field content using regular expression %sandwich_slicer_preg_pattern'),
      'field_formatters' => array(
        'text_default' => array('#markup'),
        'text_trimmed' => array('#markup'),
        'text_summary_or_trimmed' => array('#markup'),
      ),
    ),
  );
}

/**
 * Callback for preg slicer
 */
function sandwich_slicer_preg($text, $display) {
  $pattern = $display['settings']['sandwich_slicer_preg_pattern'];
  return preg_split($pattern, $text, NULL, PREG_SPLIT_NO_EMPTY);
}
