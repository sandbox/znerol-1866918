<?php

/**
 * Implements hook_preprocess_HOOK().
 *
 * Here we set the variable hamncheese_inject_fields which is subsequently used
 * by the themeing function 'hamncheese_sandwich__body__article' to inject the
 * field contents of field_image before the fourth paragraph.
 *
 * Note that we have access to the entity through the variable and therefore it
 * would be perfectly possible to dynamically adapt the place where the image
 * will appear.
 */
function hamncheese_preprocess_sandwich(&$variables) {
  $variables['hamncheese_inject_fields'] = array(
    3 => array('field_image'),
  );
}

/**
 * Overrides theme_sandwich() for body fields when the bundle name is article.
 */
function hamncheese_sandwich__body__article($variables) {
  $result = '';

  // Iterate over slices
  foreach ($variables['slices'] as $i => $markup) {

    // Check if someone told us to inject a field at this position.
    if (isset($variables['hamncheese_inject_fields'][$i])) {
      foreach ($variables['hamncheese_inject_fields'][$i] as $field_name) {
        $result .= '<div class="floatbox">';
        $result .= render($variables['entity_output'][$field_name]);
        $result .= '</div>';
      }
    }

    // Add the markup from the slide.
    $result .= $markup;
  }

  return $result;
}
