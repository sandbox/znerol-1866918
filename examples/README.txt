INSTALLATION
------------

The examples contained in this folder are themes and not modules. Place them
into your sites/all/themes folder such that Drupal can find them.
